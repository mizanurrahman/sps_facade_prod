package com.foundation.sps.facade;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;

public class SPSConsumerServicesTest extends BaseTest
{

	//private String title="MR";
	private String firstName="Robert";
	private String lastName="Sheehan";
	private String email;
	private String password="passed1";
	
	private String schoolId="8522";
	
	private boolean firstYearTeaching = true;
	private String parentPrimaryRole="2";
	private String displayName="ZigZag";
	private List<String> userType=new ArrayList<String>();
	private String consumerSPSID;
	
	
	//End Points
	private static final String ENDPOINT_CONSUMER_REGISTRATION="/spsuser/?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_CONSUMER_GET="/spsuser/{spsId}?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_CONSUMER_UPDATE="/spsuser/{spsId}?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_CONSUMER_DELETE="/spsuser/{spsId}?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_TEACHER_DELETE="/spsuser/{spsId}?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_CONSUMER_UPGRADETEACHER="/spsuser/{spsId}/upgradetoeducator?clientId=SCSToolsApp";

	@Test
	public void createConsumerGetAndDeleteTest()
	{
			System.out.println("#####################################################################################");
			System.out.println("******************** Light Consumer Registration and Get Consumer********************");
			System.out.println("#####################################################################################");
			
			
			// Light Consumer Registration
			ExtractableResponse<Response> createConsumerResponse=
								given()
										.log().all()
										.header("Authorization", String.format("Bearer %s",getAccesToken()))
										.contentType("application/json")
										.body(createConsumerPayload()).
								when()
										.post(ENDPOINT_CONSUMER_REGISTRATION).
								then()
										.statusCode(201)
										.spec(createConsumerResponseValidator())
										.extract();		
			consumerSPSID=createConsumerResponse.path("spsId");	
			System.out.println(consumerSPSID);
			
			// Get Consumer
			ExtractableResponse<Response> getConsumerResponse=
								given()
										.header("Authorization", String.format("Bearer %s",getAccesToken()))
										.pathParam("spsId",consumerSPSID).
								when()
										.get(ENDPOINT_CONSUMER_GET).
								then()
										.statusCode(200)
										.extract();		
			System.out.println(getConsumerResponse.asString());
			
			// Delete Consumer 
			ExtractableResponse<Response> deleteConsumerResponse=
								given()
										.header("Authorization", String.format("Bearer %s",getAccesToken()))
										.pathParam("spsId",consumerSPSID).
								when()
										.delete(ENDPOINT_CONSUMER_DELETE).
								then()
										.statusCode(200)
										.extract();	
			System.out.println("************** After Deleting the Consumer *****************");
			System.out.println(deleteConsumerResponse.asString());
	}	

	@Test
	public void createConsumerUpdateGetAndDeleteTest()
	{		
			System.out.println("##########################################################################################################");
			System.out.println("********************  Light Consumer Registration plus Update, Get Consumer and Delete********************");
			System.out.println("##########################################################################################################");
					
			// Light Consumer Registration
			ExtractableResponse<Response> createConsumerResponse=
								given()
										.log().all()
										.header("Authorization", String.format("Bearer %s",getAccesToken()))
										.contentType("application/json")
										.body(createConsumerPayload()).
								when()
										.post(ENDPOINT_CONSUMER_REGISTRATION).
								then()
										.statusCode(201)
										.spec(createConsumerResponseValidator())
										.extract();		
			consumerSPSID=createConsumerResponse.path("spsId");
			System.out.println(consumerSPSID);
	
			// Update Consumer 
								given()
										.log().all()
										.header("Authorization", String.format("Bearer %s",getAccesToken()))
										.pathParam("spsId",consumerSPSID)
										.contentType("application/json")
										.body(updateConsumerPayload()).
								when()
										.put(ENDPOINT_CONSUMER_UPDATE).
								then()
										.statusCode(200)
										.extract();	
			// Get Consumer
			ExtractableResponse<Response> getConsumerResponse=
								given()
										.header("Authorization", String.format("Bearer %s",getAccesToken()))
										.pathParam("spsId",consumerSPSID).
								when()
										.get(ENDPOINT_CONSUMER_GET).
								then()
										.statusCode(200)
										.spec(createConsumerUpdateResponseValidator())
										.extract();		
			System.out.println(getConsumerResponse.asString());
	
			// Delete Consumer 
			ExtractableResponse<Response> deleteConsumerResponse=
								given()
										.header("Authorization", String.format("Bearer %s",getAccesToken()))
										.pathParam("spsId",consumerSPSID).
								when()
										.delete(ENDPOINT_CONSUMER_DELETE).
								then()
										.statusCode(200)
										.extract();	
			System.out.println("************** After Deleting the Updated Consumer *****************");
			System.out.println(deleteConsumerResponse.asString());
	}
	@Test
	public void createConsumerUpgradeToTeacherGetAndDeleteTest()
	{
			System.out.println("####################################################################################################################");
			System.out.println("******************** Light Consumer Registration plus Upgrade to Teacher, Get Teacher and Delete ********************");
			System.out.println("####################################################################################################################");
				
			// Light Consumer Registration
			ExtractableResponse<Response> createConsumerResponse=
								given()
										.log().all()
										.header("Authorization", String.format("Bearer %s",getAccesToken()))
										.contentType("application/json")
										.body(createConsumerPayload()).
								when()
										.post(ENDPOINT_CONSUMER_REGISTRATION).
								then()
										.statusCode(201)
										.spec(createConsumerResponseValidator())
										.extract();		
			consumerSPSID=createConsumerResponse.path("spsId");
			System.out.println(consumerSPSID);

				// Upgrade Consumer to Teacher
			ExtractableResponse<Response> upgradeConsumerResponse=
								given()
										.log().all()
										.header("Authorization", String.format("Bearer %s",getAccesToken()))
										.pathParam("spsId",consumerSPSID)
										.contentType("application/json")
										.body(upgradeConsumerToEducatorPayload()).
								when()
										.put(ENDPOINT_CONSUMER_UPGRADETEACHER).
								then()
										.statusCode(200)
										.extract();	
	
			System.out.println("@@@@@"+upgradeConsumerResponse.asString());

			// Get Teacher
			ExtractableResponse<Response> getTeacherResponse=
								given()
										.header("Authorization", String.format("Bearer %s",getAccesToken()))
										.pathParam("spsId",consumerSPSID).
								when()
										.get(ENDPOINT_CONSUMER_GET).
								then()
										.statusCode(200)
										.spec(createConsumerUpgradeResponseValidator())
										.extract();		
			System.out.println(getTeacherResponse.asString());
	
			// Delete Teacher
			ExtractableResponse<Response> deleteTeacherResponse=
								given()
										.header("Authorization", String.format("Bearer %s",getAccesToken()))
										.pathParam("spsId",consumerSPSID).
								when()
										.delete(ENDPOINT_TEACHER_DELETE).
								then()
										.statusCode(200)
										.extract();	
			System.out.println("************** After Deleting the Consumer *****************");
			System.out.println(deleteTeacherResponse.asString());
	}	

	private String getEmail()
	{
		email="sps_qa"+String.valueOf(System.currentTimeMillis())+"@sample.com";
		return email;
		
	}
		
	private Map<String, Object> createConsumerPayload()
	{
		Map<String, Object> consumerRegistrationInfo = new HashMap<String, Object>();
		consumerRegistrationInfo.put("firstName",firstName);
		consumerRegistrationInfo.put("lastName",lastName);
		consumerRegistrationInfo.put("email",getEmail());
		userType.add("CONSUMER");
		consumerRegistrationInfo.put("userType", userType);
		consumerRegistrationInfo.put("password",password);
		consumerRegistrationInfo.put("userName",email);
		return consumerRegistrationInfo;
	}
	private Map<String, Object> updateConsumerPayload()

	{
		Map<String, Object> updateConsumerInfo = new HashMap<String, Object>();
		updateConsumerInfo.put("displayName", displayName);
		//interesParent.add("1");
		//interesParent.add("2");
		//updateConsumerInfo.put("interesParent", interesParent);
		updateConsumerInfo.put("parentPrimaryRole", parentPrimaryRole);
		return updateConsumerInfo;
	}	
	private Map<String, Object> upgradeConsumerToEducatorPayload()
	{
		Map<String, Object> upgradeConsumerToEducatorInfo = new HashMap<String, Object>();
		upgradeConsumerToEducatorInfo.put("schoolId",schoolId);
		Map<String, Object> gradesSize = new HashMap<String, Object>();
		gradesSize.put("98", "19");
		gradesSize.put("05", "21");
		upgradeConsumerToEducatorInfo.put("gradesSize", gradesSize);
		upgradeConsumerToEducatorInfo.put("firstYearTeaching", firstYearTeaching);
		return upgradeConsumerToEducatorInfo;
	}
	private ResponseSpecification createConsumerResponseValidator()
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("userName",equalTo(email))
		.expectBody("userType",equalTo(userType))
		.expectBody("email",equalTo(email))
		.expectBody("firstName",equalTo(firstName))
		.expectBody("lastName",equalTo(lastName))
		//.expectBody("password",equalTo(password))
		.expectBody("spsId",is(not(empty())));
		return rspec.build();		
	}
	private ResponseSpecification createConsumerUpdateResponseValidator()
	
	//Update Consumer Validation
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("spsId",is(not(empty())))
		.expectBody("displayName",equalTo(displayName))
		//.expectBody("interesParent",equalTo(interesParent))
		.expectBody("parentPrimaryRole",equalTo(parentPrimaryRole))
		.expectBody("userName",equalTo(email));
		
		return rspec.build();		
	}
	private ResponseSpecification createConsumerUpgradeResponseValidator()
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("userName",equalTo(email))
		//.expectBody("modifiedDate",equalTo(""))
		//.expectBody("registrationDate",equalTo(new Date()))
		.expectBody("schoolId",equalTo(schoolId))
		.expectBody("orgZip",is(not(empty())))
		//.expectBody("userType",hasItems(userType))
		.expectBody("isEducator",is(not(empty())))
		.expectBody("cac",is(not(empty())))
		.expectBody("cacId",is(not(empty())))
		.expectBody("isIdUsed",is(not(empty())))
		.expectBody("isEnabled",is(not(empty())))
		.expectBody("schoolUcn",is(not(empty())))
		.expectBody("email",equalTo(email))
		.expectBody("firstName",equalTo(firstName))
		.expectBody("lastName",equalTo(lastName))
		//.expectBody("password",equalTo(password))
		.expectBody("spsId",is(not(empty())));
		return rspec.build();		
	}
}
