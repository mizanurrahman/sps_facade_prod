package com.foundation.sps.facade;

import org.junit.Before;

import com.jayway.restassured.RestAssured;

public class BaseTest
{
	@Before
	public void init() 
	{
		
		RestAssured.baseURI="https://api.scholastic.com";
		RestAssured.port=8080;
		RestAssured.basePath = "/sps-api-facade/1.0";
		System.out.println("\n\n\n\n*******Initialized RestAssured*******");
		System.out.println("RestAssured.baseURI: " + RestAssured.baseURI);
		System.out.println("RestAssured.port: " + RestAssured.port);
		System.out.println("RestAssured.basePath: " + RestAssured.basePath); 
		System.out.println("******************************\n\n\n\n");
	}	
	
	public String getAccesToken()
	{
		return "nckCUiejFeK2C0ttN1RMPmz99Ooa";
	}

}
