package com.foundation.sps.facade;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.isEmptyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;



public class SPSLookupServicesTest extends BaseTest
{
	/*private String user ="testbhavanics15@scholastic.com";
	private String password ="password1";
	private boolean singleToken = true;
	private boolean userForWS = false;
	private boolean addCookie = true;
	private boolean addExpiration = true;

	private String sps_session;
	private String sps_tsp;*/

	private static final String ENDPOINT_ALLSTATE="/lookup/states";	
	private static final String ENDPOINT_ALLCITY="/lookup/states/NY/cities";
	private static final String ENDPOINT_ALLCOUNTRY="/lookup/countries";
	private static final String ENDPOINT_COUNTRYSCHOOLS="/lookup/countries/BD/school";
	private static final String ENDPOINT_ALLTEACHERS="/lookup/sps/school/{schoolId}/educator?fat=&";
	private static final String ENDPOINT_ALTERNATETEACHER="/lookup/sps/school/{schoolId}/educator/alternate";
	private static final String ENDPOINT_EXSISTINGSHIPPINGADDRESS="/spsaddressbook/{userId}/";
	private static final String ENDPOINT_PRIMARYSCHOOLADDRESS="/lookup/user/{userId}/organization";
	private static final String ENDPOINT_SCHOOLBYCITYSATATE="/lookup/states/NY/cities/Acra/school?forceClient=&";
	private static final String ENDPOINT_SCHOOLBYZIPCODE="/lookup/zipcode/{zipCode}/school?forceClient=&";
	private static final String ENDPOINT_EXPOSECONSUMER="/lookup/user?type=*&userType=consumer&";
	private static final String ENDPOINT_EXPOSEEDUCATOR="/lookup/user?type=*&userType=educator&";
	private static final String ENDPOINT_ONLYTEACHERS="/lookup/sps/school/{schoolId}/educator?fat=true&";
	private static final String ENDPOINT_CRIDTCARDSWALLET="/spsuser/{userId}/cybersource/creditcard";
	private static final String ENDPOINT_ALLGRADECLASSSIZE="/spsuser/{userId}/gradeclasssize";
	private static final String ENDPOINT_CHILDUSERS="/spschilduser/{childUserId}";
	private static final String ENDPOINT_BPSUMMARY="/user/bpbpsweb/SummaryServiceServlet";
	private static final String ENDPOINT_BPHISTORY="/user/bpbpsweb/HistoryServiceServlet";
	//private static final String ENDPOINT_EXISTINGASSOCIATEDSOCIALACCOUNTS="/spssocial/{userId}";
	
	@Test
	public void getAllStateTest()
	{	
		System.out.println("########################################################");
		System.out.println("******************** Get All States ********************");
		System.out.println("########################################################");
		ExtractableResponse<Response> getAllStateResponse=
								given()
										.param("clientId","SPSFacadeAPI")
										.header("Authorization", String.format("Bearer %s",getAccesToken())).
								when()
										.get(ENDPOINT_ALLSTATE).
								then()
										.statusCode(200)
										.spec(getAllStateResponseValidator())
										.extract();	
		System.out.println(getAllStateResponse.asString());
	}
	
	
	@Test
	public void getAllCityTest() 
	{
		System.out.println("#################################################################");
		System.out.println("******************** Get All Cities by State ********************");
		System.out.println("#################################################################");				
		ExtractableResponse<Response> getAllCityResponse=
								given()
										
										.param("clientId","SPSFacadeAPI")
										.header("Authorization", String.format("Bearer %s",getAccesToken())).
								when()
										.get(ENDPOINT_ALLCITY).
								then()
										.statusCode(200)
										//.spec(getAllCityResponseValidator())
										.extract();	
		System.out.println(getAllCityResponse.asString());
	}
		
	@Test
	public void getAllCountryTest() 
	{
		System.out.println("###########################################################");
		System.out.println("******************** Get All Countries ********************");
		System.out.println("###########################################################");
		
		ExtractableResponse<Response> getAllCountryResponse=
								given()
										.param("clientId","SPSFacadeAPI")
										.header("Authorization", String.format("Bearer %s",getAccesToken())).
								when()
										.get(ENDPOINT_ALLCOUNTRY).
						        then()
						        		.statusCode(200)
						        		.spec(getAllCountryResponseValidator())
						        		.extract();		
		System.out.println(getAllCountryResponse.asString());
	}

	
	@Test
	public void getCountrySchoolsTest() 
	{
		System.out.println("####################################################################");
		System.out.println("******************** Get All Schools by Country ********************");
		System.out.println("####################################################################");
		
		ExtractableResponse<Response> getCountrySchoolsResponse=
							given()
									.param("clientId","SPSFacadeAPI")
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.get(ENDPOINT_COUNTRYSCHOOLS).
							then()
									.statusCode(200)
									.spec(getCountrySchoolsResponseValidator())
									.extract();		
		System.out.println(getCountrySchoolsResponse.asString());
	}
		
		
	@Test
	public void getSchoolByCityStateTest() 
	{
		System.out.println("#######################################################################");
		System.out.println("******************** Get Schools by State and City ********************");
		System.out.println("#######################################################################");	
		
		ExtractableResponse<Response> getSchoolByCityState=
							given()
									.param("clientId","SPSFacadeAPI")
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.get(ENDPOINT_SCHOOLBYCITYSATATE).
							then()
			 						.statusCode(200)
			 						.spec(getSchoolByCityStateResponseValidator())
			 						.extract();		
		System.out.println(getSchoolByCityState.asString());
	}
	
	@Test
	public void getSchoolByZipcodeTest()
	{
		System.out.println("#################################################################");
		System.out.println("******************** Get Schools by Zip Code ********************");
		System.out.println("#################################################################");
		
		String zipCode="11431";
		
		ExtractableResponse<Response> getSchoolByZipcode=
							given()
									.param("clientId","SPSFacadeAPI")
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.get(ENDPOINT_SCHOOLBYZIPCODE, zipCode).
							then()
					 				.statusCode(200)
					 				.spec(getSchoolByZipcodeResponseValidator())
					 				.extract();		
		System.out.println(getSchoolByZipcode.asString());
	}
	
	@Test
	public void getAllTeachersTest()
	{
		System.out.println("####################################################################################");
		System.out.println("******************** Get All Teachers not logged in Readig Clubs********************");
		System.out.println("####################################################################################");
		
		String schoolId="411778";
		ExtractableResponse<Response> getAllTeachersResponse=
							given()
									.log().all()
									.param("clientId","SPSFacadeAPI")
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.get(ENDPOINT_ALLTEACHERS, schoolId).
							then()
									.statusCode(200)
									.spec(getAllTeachersResponseValidator())
									.extract();				
		System.out.println(getAllTeachersResponse.asString());
	}
	
	@Test
	public void getAlternateTeacherTest() 
	{
		System.out.println("################################################################################");
		System.out.println("******************** Get All Alternate Teachers in a School ********************");
		System.out.println("################################################################################");	
		
		String schoolId="163392";
		
		ExtractableResponse<Response> getAlternateTeacherResponse=
							given()
									.param("clientId","SPSFacadeAPI")
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.get(ENDPOINT_ALTERNATETEACHER, schoolId).
							then()
									.statusCode(200)
									.spec(getAlternateTeacherResponseValidator())
									.extract();				
		System.out.println(getAlternateTeacherResponse.asString());
			
	}
	
	@Test
	public void getExistingShippingAddressTest() 
	{
		System.out.println("#######################################################################");
		System.out.println("******************** Get Existing Shipping Address ********************");
		System.out.println("#######################################################################");
		
		String userId="4676955";
		ExtractableResponse<Response> getExistingShippingAddress=
							given()
									.param("clientId","SPSFacadeAPI")
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.get(ENDPOINT_EXSISTINGSHIPPINGADDRESS, userId).
							then()
									.statusCode(200)
									.spec(getExistingShippingAddressResponseValidator())
									.extract();		
			System.out.println(getExistingShippingAddress.asString());
	}
	
	@Test
	public void getPrimarySchoolAddressTest() 
	{
		System.out.println("####################################################################");
		System.out.println("******************** Get Primary School Address ********************");
		System.out.println("####################################################################");
		
		String userId="4676955";
		
		ExtractableResponse<Response> getPrimarySchoolAddress=
							given()
								.param("clientId","SPSFacadeAPI")
								.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
								.get(ENDPOINT_PRIMARYSCHOOLADDRESS, userId).
							then()
								.statusCode(200)
								.spec(getPrimarySchoolAddressResponseValidator())
								.extract();		
		System.out.println(getPrimarySchoolAddress.asString());
	}
		
	@Test
	public void getExposeConsumerTest()
	{
		System.out.println("##############################################################");
		System.out.println("******************** Expose Educator Data ********************");
		System.out.println("##############################################################");	
		
		ExtractableResponse<Response> getExsposeConsumerResponse=
							given()
									.param("clientId","SPSFacadeAPI")
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.get(ENDPOINT_EXPOSECONSUMER).
									then()
									.statusCode(200)
									//.spec(getExposeConsumerResponseValidator())
									.extract();		
		System.out.println(getExsposeConsumerResponse.asString());
	}
		
	@Test
	public void getExposeEducatorTest()
	{
		System.out.println("##############################################################");
		System.out.println("******************** Expose Consumer Data ********************");
		System.out.println("##############################################################");	
		
		ExtractableResponse<Response> getExposeEducatorResponse=
							given()
									.param("clientId","SPSFacadeAPI")
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.get(ENDPOINT_EXPOSEEDUCATOR).
							then()
									.statusCode(200)
									//.spec(getExposeEducatorResponseValidator())
									.extract();		
		System.out.println(getExposeEducatorResponse.asString());
	}
		
	@Test
	public void getOnlyTeachersTest()
	{
		System.out.println("###################################################################################");
		System.out.println("******************** Get Only Teachers Logged in Redading Club ********************");
		System.out.println("###################################################################################");
			
		String schoolId="411778";
			
		ExtractableResponse<Response> getOnlyTeachers=
							given()
									.param("clientId","SPSFacadeAPI")
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.get(ENDPOINT_ONLYTEACHERS, schoolId).
							then()
									.statusCode(200)
									.spec(getOnlyTeachersResponseValidator())
									.extract();				
		System.out.println(getOnlyTeachers.asString());
	}
		
	@Test
	public void getCreditCardsWalletTest() 
	{
		System.out.println("################################################################");
		System.out.println("******************** Get Credit Card Wallet ********************");
		System.out.println("################################################################");
		
		String userId="63420798";
					
		ExtractableResponse<Response> getCreditCardsWallet=
							given()
									.log().all()
									.param("clientId","SPSFacadeAPI")
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.get(ENDPOINT_CRIDTCARDSWALLET, userId).
							then()
									.statusCode(200)
									.spec(getCreditCardWalletResponseValidator())
									.extract();		
		System.out.println(getCreditCardsWallet.asString());
	}
		
	@Test
	public void getAllGradeClassSizeTest()
	{
		System.out.println("########################################################################");
		System.out.println("******************** List All Grades and Class Sizes ********************");
		System.out.println("########################################################################");
		
		String userId="68285633";
			
		ExtractableResponse<Response> getAllGradeClassSize=
							given()
									.param("clientId","SPSFacadeAPI")
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.get(ENDPOINT_ALLGRADECLASSSIZE, userId).
							then()
									.statusCode(200)
									.spec(getAllGradeClassSizeResponseValidator())
									.extract();				
		System.out.println(getAllGradeClassSize.asString());
	}
		
	@Test
	public void getChildUsersTest() 
	{
		System.out.println("########################################################");
		System.out.println("******************** Get Child User ********************");
		System.out.println("########################################################");	
			
		String childUserId="4990244";
			
		ExtractableResponse<Response> getChildUsers=
							given()
									.log().all()
									.param("clientId","SPSFacadeAPI")
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.get(ENDPOINT_CHILDUSERS, childUserId).
							then()
									.statusCode(200)
									.spec(getChildUsersResponseValidator())
									.extract();		
		System.out.println(getChildUsers.asString());
	}
	
	/*@Test
	public void createLogInAndGetBonusPointSummaryTest() 
	{	
		System.out.println("#######################################################################");
		System.out.println("***Login to Create Session Id,TSP Id and Get the Bonus Point Summary***");
		System.out.println("#######################################################################");
		//ndW9LqFqW04jJO0mGyEriFNVfVga
						
		ExtractableResponse<Response> createLogInResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createLogInPayload()).
				when()
						//.post("https://api.scholastic.com/sps-api-facade/1.0/spsuser/login?clientId=SPSFacadeAPI").
						.post("http://fs-iam-spsapifacade-prod.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
				then()
						.statusCode(200)
						.extract();	
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		
		System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		 
		//Get
		ExtractableResponse<Response> bonusPoinSummaryresponse=
				given()
						.log().all()
						.with()
						.header("Authorization", String.format("Bearer %s",getAccesToken()))
						.cookies("SPS_SESSION",sps_session)
						.cookie("SPS_TSP",sps_tsp).
				when()
						.get(ENDPOINT_BPSUMMARY).
				then()
						.statusCode(200)
						.extract();
						//.spec(createHomeSchoolResponseValidator())
		
		
		System.out.println("---------------------Bonus Point Summary--------------------------");
				
		System.out.println(bonusPoinSummaryresponse.asString());
		
	}
	
	@Test
	public void createLogInAndGetBonusPointHistoryTest() 
	{	
		System.out.println("#######################################################################");
		System.out.println("***Login to Create Session Id,TSP Id and Get the Bonus Point History***");
		System.out.println("#######################################################################");
		//ndW9LqFqW04jJO0mGyEriFNVfVga
						
		ExtractableResponse<Response> createLogInResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createLogInPayload()).
				when()
						.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
				then()
						.statusCode(200)
						.extract();	
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		
		System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		 
		//Get
		ExtractableResponse<Response> bonusPointHistoryresponse=
				given()
						.log().all()
						.with()
						.header("Authorization", String.format("Bearer %s",getAccesToken()))
						.cookies("SPS_SESSION",sps_session)
						.cookie("SPS_TSP",sps_tsp).
				when()
						.get(ENDPOINT_BPHISTORY).
				then()
						.statusCode(200)
						.extract();
						//.spec(createHomeSchoolResponseValidator())
		
		
		System.out.println("---------------------Bonus Point History--------------------------");
				
		System.out.println(bonusPointHistoryresponse.asString());
		
	
	}
	@Test
	public void getExistingAssociatedSocialAccountsTest() 
	{
		System.out.println("#######################################################################################");
		System.out.println("******************** Get Existing Associated Social Media Accounts ********************");
		System.out.println("#######################################################################################");
		
		String userId="4676955";
			
		ExtractableResponse<Response> getExistingAssociatedSocialAccounts=
							given()
									.param("clientId","SPSFacadeAPI")
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.get(ENDPOINT_EXISTINGASSOCIATEDSOCIALACCOUNTS, userId).
							then()
									.statusCode(200)
									//.spec(getExistingAssociatedSocialAccountsResponseValidator())
									.extract();		
		System.out.println(getExistingAssociatedSocialAccounts.asString());
	}*/
		
	private ResponseSpecification createEnumResponseValidator(String[][] keyvalues )
	{
		ResponseSpecBuilder respec=new ResponseSpecBuilder()
		.expectStatusCode(200)
		.expectContentType(ContentType.JSON);
		addKeyValueValidations(respec,keyvalues);
		return respec.build();
	}
	
	/*private Map<String, Object> createLogInPayload()

	{
		Map<String, Object> createLogInInfo = new HashMap<String, Object>();
		createLogInInfo.put("user",user );
		createLogInInfo.put("password",password);
		createLogInInfo.put("singleToken",singleToken);
		createLogInInfo.put("userForWS", userForWS);
		createLogInInfo.put("addCookie",addCookie);
		createLogInInfo.put("addExpiration",addExpiration);
		return createLogInInfo;
	
	}*/
	private ResponseSpecification getAllStateResponseValidator()
	{
		final String[][] keyValues = 
		{ 
			{ "AK", "AK - Alaska" }, { "AL", "AL - Alabama" }, { "AR", "AR - Arkansas" },
			{ "AS", "AS - American Samoa" }, { "AZ", "AZ - Arizona" },{"CA","CA - California" }, { "CO", "CO - Colorado" },
			{ "CT", "CT - Connecticut" }, { "DC", "DC - District of Columbia" }, { "DE", "DE - Delaware" }, 
			{ "FL", "FL - Florida" }, { "FM", "FM - Micronesia" }, { "GA", "GA - Georgia" }, { "GU", "GU - Guam" },
			{ "HI", "HI - Hawaii" }, { "IA", "IA - Iowa" }, { "ID", "ID - Idaho" }, { "IL", "IL - Illinois" }, 
			{ "IN", "IN - Indiana" }, { "KS", "KS - Kansas" }, { "KY", "KY - Kentucky" }, { "LA", "LA - Louisiana"}, 
			{ "MA", "MA - Massachusetts" }, { "MD", "MD - Maryland" }, { "ME", "ME - Maine" }, { "MH", "MH - Marshall Isl"},
			{ "MI", "MI - Michigan" }, { "MN", "MN - Minnesota" }, { "MO", "MO - Missouri" }, { "MP", "MP - Northern Mariana Isls" },
			{ "MS", "MS - Mississippi" }, { "MT", "MT - Montana" }, { "NC", "NC - North Carolina" }, { "ND", "ND - North Dakota" },
			{ "NE", "NE - Nebraska" }, { "NH", "NH - New Hampshire" }, { "NJ", "NJ - New Jersey" }, { "NM", "NM - New Mexico" },
			{ "NV", "NV - Nevada" }, { "NY", "NY - New York" }, { "OH", "OH - Ohio" }, { "OK", "OK - Oklahoma" }, { "OR", "OR - Oregon" },
			{ "PA", "PA - Pennsylvania" }, { "PR", "PR - Puerto Rico" }, { "PW", "PW - Palau" }, { "RI", "RI - Rhode Island" }, 
			{ "SC", "SC - South Carolina" }, { "SD", "SD - South Dakota" }, { "TN", "TN - Tennessee" }, { "TX", "TX - Texas" }, 
			{ "UM", "UM - US Minor Outlying Isls" }, { "UT", "UT - Utah" }, { "VA", "VA - Virginia" }, { "VI", "VI - Virgin Islands" },
			{ "VT", "VT - Vermont" }, { "WA", "WA - Washington" }, { "WI", "WI - Wisconsin" }, { "WV", "WV - West Virginia" }, { "WY", "WY - Wyoming" }
		};
		return createEnumResponseValidator(keyValues);
	}
		
		
		
	private ResponseSpecification getAllCountryResponseValidator()
	{
		final String[][] keyValues = 
		{ 
			 { "AD", "Andorra" }, { "AE", "United Arab Emirates" }, { "AF", "Afghanistan" }, { "AG", "Antigua And Barbuda" }, { "AI", "Anguilla" },
			 { "AL", "Albania" }, { "AM", "Armenia" }, { "AN", "Netherlands Antilles" }, { "AO", "Angola" }, { "AQ", "Antarctica" }, { "AR", "Argentina" },
			 { "AT", "Austria" }, { "AU", "Australia" }, { "AW", "Aruba" }, { "AX", "And Islands" }, { "AZ", "Azerbaijan" }, { "BA", "Bosnia And Herzegovina" },
			 { "BB", "Barbados" }, { "BD", "Bangladesh" }, { "BE", "Belgium" }, { "BF", "Burkina Faso" }, { "BG", "Bulgaria" }, { "BH", "Bahrain" }, 
			 { "BI", "Burundi" }, { "BJ", "Benin" }, { "BM", "Bermuda" }, { "BN", "Brunei Darussalam" }, { "BO", "Bolivia" }, { "BR", "Brazil" }, 
			 { "BS", "Bahamas" }, { "BT", "Bhutan" }, { "BV", "Bouvet Island" }, { "BW", "Botswana" }, { "BY", "Belarus" }, { "BZ", "Belize" },
			 { "CA", "Canada" }, { "CC", "Cocos (keeling) Islands" }, { "CD", "Congo, Democratic Rep Of" }, { "CF", "Central African Republic" },
			 { "CG", "Congo" }, { "CH", "Switzerland" }, { "CI", "Cote D'ivoire" }, { "CK", "Cook Islands" }, { "CL", "Chile" }, { "CM", "Cameroon" },
			 { "CN", "China" }, { "CO", "Colombia" }, { "CR", "Costa Rica" }, { "CS", "Serbia And Montenegro" }, { "CV", "Cape Verde" }, { "CX", "Christmas Island" },
			 { "CY", "Cyprus" }, { "CZ", "Czech Republic" }, { "DE", "Germany" }, { "DJ", "Djibouti" }, { "DK", "Denmark" }, { "DM", "Dominica" }, 
			 { "DO", "Dominican Republic" }, { "DZ", "Algeria" }, { "EC", "Ecuador" }, { "EE", "Estonia" }, { "EG", "Egypt" }, { "EH", "Western Sahara" },
			 { "ER", "Eritrea" }, { "ES", "Spain" }, { "ET", "Ethiopia" }, { "FI", "Finland" }, { "FJ", "Fiji" }, { "FK", "Falkland Islands" }, 
			 { "FM", "Micronesia, Fed States Of" }, { "FO", "Faroe Islands" }, { "FR", "France" }, { "GA", "Gabon" }, { "GB", "United Kingdom" }, 
			 { "GD", "Grenada" }, { "GE", "Georgia" }, { "GF", "French Guiana" }, { "GH", "Ghana" }, { "GI", "Gibraltar" }, { "GL", "Greenland" }, 
			 { "GM", "Gambia" }, { "GN", "Guinea" }, { "GP", "Guadeloupe" }, { "GQ", "Equatorial Guinea" }, { "GR", "Greece" }, { "GS", "S Georgia-s Sandwich Isls" },
			 { "GT", "Guatemala" }, { "GW", "Guinea-bissau" }, { "GY", "Guyana" }, { "HK", "Hong Kong" }, { "HM", "Heard Isl-mcdonald Isl" }, 
			 { "HN", "Honduras" }, { "HR", "Croatia" }, { "HT", "Haiti" }, { "HU", "Hungary" }, { "ID", "Indonesia" }, { "IE", "Ireland" },
			 { "IL", "Israel" }, { "IN", "India" }, { "IO", "British Indian Ocean Terr" }, { "IQ", "Iraq" }, { "IS", "Iceland" }, { "IT", "Italy" }, { "JM", "Jamaica" },
			 { "JO", "Jordan" }, { "JP", "Japan" }, { "KE", "Kenya" }, { "KG", "Kyrgyzstan" }, { "KH", "Cambodia" }, { "KI", "Kiribati" }, { "KM", "Comoros" }, 
			 { "KN", "Saint Kitts And Nevis" }, { "KR", "Korea, Republic Of" }, { "KW", "Kuwait" }, { "KY", "Cayman Islands" }, { "KZ", "Kazakhstan" }, 
			 { "LA", "Lao People's Dem Rep" }, { "LB", "Lebanon" }, { "LC", "Saint Lucia" }, { "LI", "Liechtenstein" }, { "LK", "Sri Lanka" }, { "LR", "Liberia" }, 
			 { "LS", "Lesotho" }, { "LT", "Lithuania" }, { "LU", "Luxembourg" }, { "LV", "Latvia" }, { "MA", "Morocco" }, { "MC", "Monaco" }, { "MD", "Moldova, Republic Of" },
			 { "ME", "Montenegro" }, { "MG", "Madagascar" },  { "MH", "Marshall Islands" }, { "MK", "Macedonia, The Fyr Of" }, { "ML", "Mali" }, { "MN", "Mongolia" }, 
			 { "MO", "Macao" }, { "MQ", "Martinique" },  { "MR", "Mauritania" }, { "MS", "Montserrat" }, { "MT", "Malta" }, { "MU", "Mauritius" }, { "MV", "Maldives" },
			 { "MW", "Malawi" }, { "MX", "Mexico" }, { "MY", "Malaysia" }, { "MZ", "Mozambique" }, { "NA", "Namibia" }, { "NC", "New Caledonia" }, { "NE", "Niger" }, 
			 { "NF", "Norfolk Island" }, { "NG", "Nigeria" }, { "NI", "Nicaragua" }, { "NL", "Netherlands" }, { "NO", "Norway" }, { "NP", "Nepal" }, { "NR", "Nauru" },
			 { "NU", "Niue" }, { "NZ", "New Zealand" }, { "OM", "Oman" }, { "PA", "Panama" }, { "PE", "Peru" }, { "PF", "French Polynesia" }, { "PG", "Papua New Guinea" }, 
			 { "PH", "Philippines" }, { "PK", "Pakistan" }, { "PL", "Poland" }, { "PM", "Saint Pierre And Miquelon" }, { "PN", "Pitcairn" }, { "PR", "Puerto Rico" }, 
			 { "PS", "Palestinian Territory" }, { "PT", "Portugal" }, { "PW", "Palau" }, { "PY", "Paraguay" }, { "QA", "Qatar" }, { "RE", "Reunion" }, { "RO", "Romania" }, 
			 { "RS", "Serbia" }, { "RU", "Russian Federation" }, { "RW", "Rwanda" }, { "SA", "Saudi Arabia" }, { "SB", "Solomon Islands" }, { "SC", "Seychelles" }, 
			 { "SE", "Sweden" }, { "SG", "Singapore" }, { "SH", "Saint Helena" }, { "SI", "Slovenia" }, { "SJ", "Svalbard And Jan Mayen" }, { "SK", "Slovakia" }, 
			 { "SL", "Sierra Leone" }, { "SM", "San Marino" }, { "SN", "Senegal" }, { "SO", "Somalia" }, { "SR", "Suriname" }, { "ST", "Sao Tome And Principe" }, 
			 { "SV", "El Salvador" }, { "SZ", "Swaziland" }, { "TC", "Turks And Caicos Islands" }, { "TD", "Chad" }, { "TF", "French Southern Terr" }, { "TG", "Togo" },
			 { "TH", "Thailand" }, { "TJ", "Tajikistan" }, { "TK", "Tokelau" }, { "TL", "Timor-leste" }, { "TM", "Turkmenistan" }, { "TN", "Tunisia" }, { "TO", "Tonga" },
			 { "TR", "Turkey" }, { "TT", "Trinidad And Tobago" }, { "TV", "Tuvalu" }, { "TW", "Taiwan" }, { "TZ", "Tanzania, United Rep Of" }, { "UA", "Ukraine" },
			 { "UG", "Uganda" }, { "US", "United States Of America" }, { "UY", "Uruguay" }, { "UZ", "Uzbekistan" }, { "VA", "Holy See (vatican)" }, 
			 { "VC", "St Vincent-grenadines" }, { "VE", "Venezuela" }, { "VG", "Virgin Islands, British" }, { "VN", "Viet Nam" }, { "VU", "Vanuatu" }, 
			 { "WF", "Wallis And Futuna" }, { "WS", "Samoa" }, { "YE", "Yemen" }, { "YT", "Mayotte" }, { "ZA", "South Africa" }, { "ZM", "Zambia" }, { "ZW", "Zimbabwe" } 
		};
		return createEnumResponseValidator(keyValues);
	}
		
			
	private ResponseSpecification getCountrySchoolsResponseValidator()
				
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("schoolUCN[0]",equalTo("600222805"))
		.expectBody("schoolStatus[0]",equalTo("1"))
		.expectBody("groupType[0]",equalTo("I_CD"))
		.expectBody("creditCardState[0]",isEmptyString())
		.expectBody("zipcode[0]",isEmptyString())
		.expectBody("address1[0]",equalTo("UNITED NATION ROAD"))
		.expectBody("city[0]",isEmptyString())
		.expectBody("name[0]",equalTo("AMER INTL SCH DHAKA"))
		.expectBody("state[0]",isEmptyString())
		.expectBody("spsId[0]",equalTo("30128319"))
		
		.expectBody("schoolUCN[1]",equalTo("628896389"))
		.expectBody("schoolStatus[1]",equalTo("1"))
		.expectBody("groupType[1]",equalTo("I_CD"))
		.expectBody("creditCardState[1]",isEmptyString())
		.expectBody("zipcode[1]",isEmptyString())
		.expectBody("address1[1]",equalTo("12 UNITED NATIONS ROAD"))
		.expectBody("city[1]",isEmptyString())
		.expectBody("name[1]",equalTo("AMERICAN INTL SCHOOL DHAKA"))
		.expectBody("state[1]",isEmptyString())
		.expectBody("spsId[1]",equalTo("30961021"))
		
		.expectBody("schoolUCN[2]",equalTo("613432361"))
		.expectBody("schoolStatus[2]",equalTo("1"))
		.expectBody("groupType[2]",equalTo("I_CD"))
		.expectBody("creditCardState[2]",isEmptyString())
		.expectBody("zipcode[2]",isEmptyString())
		.expectBody("address1[2]",equalTo("HOUSE SE5 ROAD 135"))
		.expectBody("city[2]",isEmptyString())
		.expectBody("name[2]",equalTo("AUSTRALIAN INTL SCHOOL DHAKA"))
		.expectBody("state[2]",isEmptyString())
		.expectBody("spsId[2]",equalTo("30404301"))

		.expectBody("schoolUCN[3]",equalTo("600340507"))
		.expectBody("schoolStatus[3]",equalTo("1"))
		.expectBody("groupType[3]",equalTo("I_CD"))
		.expectBody("creditCardState[3]",isEmptyString())
		.expectBody("zipcode[3]",equalTo("4000"))
		.expectBody("address1[3]",equalTo("321 SARSON ROAD"))
		.expectBody("city[3]",equalTo("CHITTAGONG"))
		.expectBody("name[3]",equalTo("CHITTAGONG GRAMMAR SCHOOL"))
		.expectBody("state[3]",isEmptyString())
		.expectBody("spsId[3]",equalTo("30152192"))
					
		.expectBody("schoolUCN[4]",equalTo("624225099"))
		.expectBody("schoolStatus[4]",equalTo("1"))
		.expectBody("groupType[4]",equalTo("I_CD"))
		.expectBody("creditCardState[4]",isEmptyString())
		.expectBody("zipcode[4]",isEmptyString())
		.expectBody("address1[4]",equalTo("HOUSE 8/A ROAD 143"))
		.expectBody("city[4]",isEmptyString())
		.expectBody("name[4]",equalTo("STS EDUCATIONAL GROUP LTD"))
		.expectBody("state[4]",isEmptyString())
		.expectBody("spsId[4]",equalTo("30786442"));
		return rspec.build();			
	}

	private ResponseSpecification getSchoolByCityStateResponseValidator()
				
	{ 
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("creditCardState[0]",equalTo("NY"))
		.expectBody("zipcode[0]",equalTo("12405"))
		.expectBody("schoolStatus[0]",equalTo("1"))
		.expectBody("groupType[0]",equalTo("EC_KN"))
		.expectBody("address1[0]",equalTo("97 MAPLE AVE"))
		.expectBody("city[0]",equalTo("ACRA"))
		.expectBody("schoolUCN[0]",equalTo("613636341"))
		.expectBody("name[0]",equalTo("HOUGHTALING DAYCARE"))
		.expectBody("state[0]",equalTo("NY"))
		.expectBody("spsId[0]",equalTo("30423484"));
		return rspec.build();
	}

					
		private ResponseSpecification getExistingShippingAddressResponseValidator(  )
		{			
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("spsId",equalTo("4676955"))
		.rootPath("objectList")
		.expectBody("id[0]",equalTo("1"))
		.expectBody("addressBookZip[0]",equalTo("11377"))
		.expectBody("isHome[0]",isEmptyString())
		.expectBody("addressBookNickName[0]",equalTo("home"))
		.expectBody("addressBookAddress1[0]",equalTo("4311 58TH ST"))
		.expectBody("addressBookAddress2[0]",isEmptyString())
		.expectBody("addressBookAddress3[0]",isEmptyString())
		.expectBody("addressBookAddress4[0]",isEmptyString())
		.expectBody("addressBookAddress5[0]",isEmptyString())
		.expectBody("addressBookFirstName[0]",equalTo("Mohammed"))
		.expectBody("addressBookLastName[0]",equalTo("Rahman"))
		.expectBody("addressBookCity[0]",equalTo("WOODSIDE"))
		.expectBody("addressBookState[0]",equalTo("NY"))
		.expectBody("addressBookPhone[0]",equalTo("|"))
		.expectBody("addressBookCountry[0]",equalTo("US"))

		.expectBody("id[1]",equalTo("3"))
		.expectBody("addressBookZip[1]",equalTo("11372"))
		.expectBody("isHome[1]",equalTo("0"))
		//.expectBody("addressBookPOBox[1]",isEmptyString())
		.expectBody("addressBookNickName[1]",equalTo("home"))
		.expectBody("addressBookAddress1[1]",equalTo("234454 vbggbsgbsd"))
		.expectBody("addressBookAddress2[1]",isEmptyString())
		.expectBody("addressBookAddress3[1]",isEmptyString())
		.expectBody("addressBookAddress4[1]",isEmptyString())
		.expectBody("addressBookAddress5[1]",isEmptyString())
		.expectBody("addressBookFirstName[1]",equalTo("Mohammed"))
		.expectBody("addressBookLastName[1]",equalTo("Rahman"))
		.expectBody("addressBookCity[1]",equalTo("Jackson Heights"))
		.expectBody("addressBookState[1]",equalTo("NY"))
		//.expectBody("addressBookEmail[1]",isEmptyString())
		//.expectBody("addressBookPhone[1]",equalTo("|"))
		.expectBody("addressBookCountry[1]",equalTo("US"))
		
		.expectBody("id[2]",equalTo("4"))
		.expectBody("addressBookZip[2]",equalTo("12345"))
		//.expectBody("isHome[2]",isEmptyString())
		.expectBody("isHome[2]",equalTo("0"))
		.expectBody("addressBookNickName[2]",equalTo("gjv"))
		.expectBody("addressBookAddress1[2]",equalTo("knn"))
		.expectBody("addressBookAddress2[2]",isEmptyString())
		.expectBody("addressBookAddress3[2]",isEmptyString())
		.expectBody("addressBookAddress4[2]",isEmptyString())
		.expectBody("addressBookAddress5[2]",isEmptyString())
		.expectBody("addressBookFirstName[2]",equalTo("khj"))
		//.expectBody("addressBooklastName[2]",equalTo("null"))
		.expectBody("addressBookCity[2]",equalTo("Schenectady"))
		.expectBody("addressBookState[2]",equalTo("NY"))
		.expectBody("addressBookPhone[2]",equalTo("8574992479|"))
		.expectBody("addressBookCountry[2]",equalTo("US"));
		return rspec.build();
	}

	private ResponseSpecification getOnlyTeachersResponseValidator()
		
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("[0].modifiedDate",equalTo("20140728"))
		.expectBody("password[0]",equalTo("iW+yjsIGgrmgFEaLqvuQxw=="))
		.expectBody("readingLevel[0]",equalTo("20"))
		.expectBody("salutation[0]",equalTo("MS"))
		.expectBody("grades[0]",hasItems("10", "09", "04", "05", "06", "07", "11", "12"))
		.expectBody("schoolId[0]",equalTo("411778"))
		.expectBody("cac[0]",equalTo("MKZVL"))
		.expectBody("userType[0]",hasItem("TEACHER"))
		.expectBody("isEducator[0]",equalTo("1"))
		.expectBody("cacId[0]",equalTo("8798181"))
		.expectBody("isIdUsed[0]",equalTo("2"))
		//.expectBody("termsConditionsAcceptedVersion[0]",equalTo("1.2"))
		//.expectBody("termsConditionsAcceptedTimestamp[0]",equalTo("Oct 16, 2015, 4:21 PM (EDT)"))
		.expectBody("mentorTeacher[0]",equalTo("1"))
		.expectBody("usedBookclubs[0]",equalTo("0"))
		.expectBody("gradeClasssizeId[0]",hasItems("1750395", "1750396", "1750393", "1750394", "1750392", "1750399", "1750398", "1750397"))
		.expectBody("firstLoginSchoolYear[0]",isEmptyString())
		.expectBody("ucn[0]",equalTo("617224016"))
		.expectBody("schoolUcn[0]",equalTo("600190609"))
		.expectBody("email[0]",equalTo("denise.mouzakis@gmail.com"))
		.expectBody("firstName[0]",equalTo("Denise"))
		.expectBody("lastName[0]",equalTo("Mouzakis"))
		.expectBody("birthMonth[0]",equalTo("07"))
		.expectBody("birthDay[0]",equalTo("02"))
		//.expectBody("privacyPolicyAcceptedVersion[0]",equalTo("1.2"))
		.expectBody("orgZip[0]",equalTo("11426"))
		.expectBody("positions[0]",hasItems("10037", "10034", "10033", "10009"))
		.expectBody("emailCaptureDate[0]",equalTo("2013-10-06"))
		.expectBody("level1RegistrationDate[0]",equalTo("Oct 06, 2013, 10:40 AM (EDT)"))
		.expectBody("level2RegistrationDate[0]",equalTo("Oct 06, 2013, 10:40 AM (EDT)"))
		.expectBody("lastmodifiedDate[0]",equalTo("Jul 28, 2014, 8:53 PM (EDT)"))
		.expectBody("isEnabled[0]",equalTo("1"))
		.expectBody("newTeacher[0]",equalTo("0"))
		.expectBody("registrationDate[0]",equalTo("Oct 06, 2013, 10:40 AM (EDT)"))
		//.expectBody("privacyPolicyAcceptedTimestamp[0]",equalTo("Oct 16, 2015, 4:21 PM (EDT)"))
		.expectBody("userName[0]",equalTo("denise.mouzakis@gmail.com"))
		.expectBody("spsId[0]",equalTo("58277564"))
		
		.expectBody("[1].modifiedDate",equalTo("20160724"))
		.expectBody("password[1]",equalTo("2TL2DOrBH/efUOpAZpgacA=="))
		.expectBody("readingLevel[1]",equalTo("20"))
		.expectBody("salutation[1]",equalTo("MR"))
		.expectBody("grades[1]",hasItems("10", "08", "09", "03", "11", "12"))
		.expectBody("schoolId[1]",equalTo("411778"))
		.expectBody("cac[1]",equalTo("PRR7N"))
		.expectBody("userType[1]",hasItem("TEACHER"))
		.expectBody("isEducator[1]",equalTo("1"))
		.expectBody("cacId[1]",equalTo("9974303"))
		.expectBody("isIdUsed[1]",equalTo("2"))
		.expectBody("termsConditionsAcceptedVersion[1]",equalTo("1.1"))
		//.expectBody("termsConditionsAcceptedTimestamp[1]",equalTo("Feb 08, 2016, 1:46 PM (EST)"))
		.expectBody("mentorTeacher[1]",equalTo("0"))
		.expectBody("usedBookclubs[1]",equalTo("1"))
		//.expectBody("gradeClasssizeId[1]",hasItems("4075811", "4075812", "4075810", "4075814", "4075809", "4075813", "4075815"))
		.expectBody("firstLoginSchoolYear[1]",equalTo(""))
		.expectBody("ucn[1]",equalTo("637100329"))
		.expectBody("schoolUcn[1]",equalTo("600190609"))
		.expectBody("email[1]",equalTo("sjohn11@schools.nyc.gov"))
		.expectBody("firstName[1]",equalTo("Sylvester"))
		.expectBody("lastName[1]",equalTo("John"))
		.expectBody("birthMonth[1]",equalTo("04"))
		.expectBody("birthDay[1]",equalTo("14"))
		.expectBody("privacyPolicyAcceptedVersion[1]",equalTo("1.1"))
		.expectBody("orgZip[1]",equalTo("11426"))
		//.expectBody("positions[1]",hasItem("10036"))
		//.expectBody("emailCaptureDate[1]",equalTo("2016-02-08"))
		.expectBody("level1RegistrationDate[1]",equalTo("Feb 08, 2016, 1:46 PM (EST)"))
		//.expectBody("level2RegistrationDate[1]",equalTo("Oct 06, 2013, 10:40 AM (EDT)"))
		//.expectBody("lastmodifiedDate[1]",equalTo("Apr 24, 2016, 6:12 PM (EDT)"))
		.expectBody("isEnabled[1]",equalTo("1"))
		.expectBody("newTeacher[1]",equalTo("1"))
		.expectBody("registrationDate[1]",equalTo("Feb 08, 2016, 1:46 PM (EST)"))
		//.expectBody("privacyPolicyAcceptedTimestamp[1]",equalTo("Feb 08, 2016, 1:46 PM (EST)"))
		.expectBody("userName[1]",equalTo("sjohn11@schools.nyc.gov"))
		.expectBody("spsId[1]",equalTo("67246800"));
		return rspec.build();
	}
		
	private ResponseSpecification getAllTeachersResponseValidator()
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("[0].modifiedDate",equalTo("20140728"))
		.expectBody("password[0]",equalTo("iW+yjsIGgrmgFEaLqvuQxw=="))
		.expectBody("readingLevel[0]",equalTo("20"))
		.expectBody("salutation[0]",equalTo("MS"))
		.expectBody("grades[0]",hasItems("10", "09", "04", "05", "06", "07", "11", "12"))
		.expectBody("schoolId[0]",equalTo("411778"))
		.expectBody("cac[0]",equalTo("MKZVL"))
		.expectBody("userType[0]",hasItem("TEACHER"))
		.expectBody("isEducator[0]",equalTo("1"))
		.expectBody("cacId[0]",equalTo("8798181"))
		.expectBody("isIdUsed[0]",equalTo("2"))
		//.expectBody("termsConditionsAcceptedVersion[0]",equalTo("1.2"))
		//.expectBody("termsConditionsAcceptedTimestamp[0]",equalTo("Oct 16, 2015, 4:21 PM (EDT)"))
		.expectBody("mentorTeacher[0]",equalTo("1"))
		.expectBody("usedBookclubs[0]",equalTo("0"))
		.expectBody("gradeClasssizeId[0]",hasItems("1750395", "1750396", "1750393", "1750394", "1750392", "1750399", "1750398", "1750397"))
		.expectBody("firstLoginSchoolYear[0]",equalTo(""))
		.expectBody("ucn[0]",equalTo("617224016"))
		.expectBody("schoolUcn[0]",equalTo("600190609"))
		.expectBody("email[0]",equalTo("denise.mouzakis@gmail.com"))
		.expectBody("firstName[0]",equalTo("Denise"))
		.expectBody("lastName[0]",equalTo("Mouzakis"))
		.expectBody("birthMonth[0]",equalTo("07"))
		.expectBody("birthDay[0]",equalTo("02"))
		//.expectBody("privacyPolicyAcceptedVersion[0]",equalTo("1.2"))
		.expectBody("bonusPoints[0]",equalTo("0"))
		.expectBody("orgZip[0]",equalTo("11426"))
		.expectBody("positions[0]",hasItems("10037", "10034", "10033", "10009"))
		.expectBody("emailCaptureDate[0]",equalTo("2013-10-06"))
		.expectBody("level1RegistrationDate[0]",equalTo("Oct 06, 2013, 10:40 AM (EDT)"))
		.expectBody("level2RegistrationDate[0]",equalTo("Oct 06, 2013, 10:40 AM (EDT)"))
		.expectBody("lastmodifiedDate[0]",equalTo("Jul 28, 2014, 8:53 PM (EDT)"))
		.expectBody("isEnabled[0]",equalTo("1"))
		.expectBody("newTeacher[0]",equalTo("0"))
		.expectBody("registrationDate[0]",equalTo("Oct 06, 2013, 10:40 AM (EDT)"))
	    //.expectBody("privacyPolicyAcceptedTimestamp[0]",equalTo("Oct 16, 2015, 4:21 PM (EDT)"))
		.expectBody("userName[0]",equalTo("denise.mouzakis@gmail.com"))
		.expectBody("spsId[0]",equalTo("58277564"))
						
		.expectBody("schoolId[1]",equalTo("411778"))
		.expectBody("schoolUcn[1]",equalTo("600190609"))
		.expectBody("orgZip[1]",equalTo("11426"))
		.expectBody("positions[1]",hasItem("10009"))
		.expectBody("salutation[1]",equalTo("MRS"))
		.expectBody("grades[1]",hasItem("00"))
		.expectBody("email[1]",equalTo("ktkilduff@optonline.net"))
		.expectBody("firstName[1]",equalTo("kathleen"))
		.expectBody("lastName[1]",equalTo("kilduff"))
		.expectBody("bcoe[1]",equalTo("1236292486"))
		.expectBody("cac[1]",equalTo("2I1GI"))
		.expectBody("corpStatus[1]",isEmptyString())
		.expectBody("userType[1]",hasItem("TEACHER"))
		.expectBody("ucn[1]",equalTo("616252884"))
		.expectBody("itRoles[1]",hasItem("0283"))
		.expectBody("emailCaptureDate[1]",equalTo("2009-09-15"))
		.expectBody("level1RegistrationDate[1]",equalTo("Sep 15, 2009, 8:09 PM (EDT)"))
		.expectBody("lastmodifiedDate[1]",equalTo("Jul 15, 2013, 7:37 PM (EDT)"))
		.expectBody("appSourceCode[1]",equalTo("Printables"))
		.expectBody("bonusPointsYtd[1]",isEmptyString())
		.expectBody("isEducator[1]",equalTo("1"))
		.expectBody("subscriptionCool[1]",equalTo("1"))
		.expectBody("subscriptionEducator[1]",equalTo("1"))
		.expectBody("bonusPoints[1]",equalTo("0"))
		.expectBody("modifiedDate[1]",equalTo("20130715"))
		.expectBody("registrationDate[1]",equalTo("Sep 15, 2009, 8:09 PM (EDT)"))
		.expectBody("isEnabled[1]",equalTo("1"))
		.expectBody("note[1]" ,hasItems("7340423|myAccountApp|Sep 15, 2009, 8:09:47 PM (EDT)|SYS: Creation",
		  				"7340423|MyAccountWebApp|Jul 27, 2011, 7:34:08 PM (EDT)|SYS: Modification:<br>Bonus Points YTD: [0]->[]",
		  				"7340423|MyAccountWebApp|Aug 05, 2011, 9:48:22 AM (EDT)|SYS: Modification:<br>Capture Date",
		  				"7340423|csiMessageApp|Aug 03, 2010, 10:27:29 PM (EDT)|SYS: Modification:<br>IT Roles",
		  				"7340423|myAccountApp|Sep 15, 2009, 8:10:12 PM (EDT)|SYS: Modification:<br>Grades<br>Position(s)<br>COOL Subscription: true<br>Educator Subscription\n\t\t\t\n\t\t: true"))
		.expectBody("userName[1]",equalTo("ktkilduff"))
		.expectBody("password[1]",equalTo("PbuPvbkBbIk="))
		.expectBody("spsId[1]",equalTo("7340423"))
	  
	  
		.expectBody("schoolId[2]",equalTo("411778"))
		.expectBody("schoolUcn[2]",equalTo("600190609"))
		.expectBody("orgZip[2]",equalTo("11426"))
		//.expectBody("positions[2]",hasItem("10036"))
		.expectBody("birthMonth[2]",equalTo("04"))
		.expectBody("birthDay[2]",equalTo("14"))
		.expectBody("schlAppUserType[2]",hasItem("2"))
		.expectBody("readingLevel[2]",equalTo("20"))
		.expectBody("salutation[2]",equalTo("MR"))
		.expectBody("grades[2]",hasItems("10", "08", "09", "03", "11", "12"))
		.expectBody("email[2]",equalTo("sjohn11@schools.nyc.gov"))
		.expectBody("firstName[2]",equalTo("Sylvester"))
		.expectBody("lastName[2]",equalTo("John"))
		.expectBody("bcoe[2]",equalTo("2130082122"))
		.expectBody( "cac[2]",equalTo("PRR7N"))
		.expectBody("corpStatus[2]",isEmptyString())
		.expectBody("privacyPolicyAcceptedVersion[2]",equalTo("1.1"))
		.expectBody("userType[2]",hasItem("TEACHER"))
		.expectBody("ucn[2]",equalTo("637100329"))
		//.expectBody("emailCaptureDate[2]",equalTo("2016-02-08"))
		.expectBody("level1RegistrationDate[2]",equalTo("Feb 08, 2016, 1:46 PM (EST)"))
		//.expectBody("lastmodifiedDate[2]",equalTo("Apr 24, 2016, 6:12 PM (EDT)"))
		//.expectBody("bonusPointsYtd[2]",equalTo("205"))
		.expectBody("isEducator[2]",equalTo("1"))
		.expectBody("cacId[2]",equalTo("9974303"))
		.expectBody("isIdUsed[2]",equalTo("2"))
		.expectBody("termsConditionsAcceptedVersion[2]",equalTo("1.1"))
		//.expectBody("termsConditionsAcceptedTimestamp[2]",equalTo("Feb 08, 2016, 1:46 PM (EST)"))
		.expectBody("enterpriseEmail[2]",equalTo("SJOHN11@SCHOOLS.NYC.GOV"))
		.expectBody("bonusPoints[2]",equalTo("205"))
		.expectBody("newTeacher[2]",equalTo("1"))
		.expectBody("modifiedDate[2]",equalTo("20160724"))
		.expectBody("registrationDate[2]",equalTo("Feb 08, 2016, 1:46 PM (EST)"))
		.expectBody("firstLoginSchoolYear[2]",equalTo(""))
		.expectBody("mentorTeacher[2]",equalTo("0"))
		.expectBody("usedBookclubs[2]",equalTo("1"))
		//.expectBody("gradeClasssizeId[2]",hasItems("4075811", "4075812", "4075810", "4075814", "4075809", "4075813", "4075815"))
		//.expectBody("privacyPolicyAcceptedTimestamp[2]",equalTo("Feb 08, 2016, 1:46 PM (EST)"))
		.expectBody("isEnabled[2]",equalTo("1"))
		.expectBody("userName[2]",equalTo("sjohn11@schools.nyc.gov"))
		.expectBody("password[2]",equalTo("2TL2DOrBH/efUOpAZpgacA=="))
		.expectBody("spsId[2]",equalTo("67246800"))
		        
		.expectBody("schoolId[3]",equalTo("411778"))
		.expectBody("schoolUcn[3]",equalTo("600190609"))
		.expectBody("orgZip[3]",equalTo("11426"))
		.expectBody("positions[3]",hasItem(""))
		.expectBody("birthMonth[3]",equalTo("02"))
		.expectBody("birthDay[3]",equalTo("07"))
		.expectBody("readingLevel[3]",equalTo("00"))
		.expectBody("salutation[3]",equalTo("MS"))
		.expectBody("grades[3]",hasItem("00"))
		.expectBody("email[3]",equalTo("bfcrane@yahoo.com"))
		.expectBody("firstName[3]",equalTo("bonnie"))
		.expectBody("lastName[3]",equalTo("crane"))
		.expectBody("bcoe[3]",equalTo("1187357114"))
		.expectBody("cac[3]",equalTo("NTXM4"))
		.expectBody("corpStatus[3]",isEmptyString())
		.expectBody("privacyPolicyAcceptedVersion[3]",equalTo("1.1"))
		.expectBody("userType[3]",hasItem("TEACHER"))
		.expectBody("ucn[3]",equalTo("615759657"))
		.expectBody("csrEmailed[3]",equalTo("B"))
		.expectBody("emailCaptureDate[3]",equalTo("2014-12-19"))
		.expectBody("level1RegistrationDate[3]",equalTo("Dec 19, 2014, 11:41 AM (EST)"))
		.expectBody("lastmodifiedDate[3]",equalTo("Jul 03, 2015, 9:50 AM (EDT)"))
		.expectBody("bonusPointsYtd[3]",equalTo("0"))
		.expectBody("isEducator[3]",equalTo("1"))
		.expectBody("cacId[3]",equalTo("9465770"))
		.expectBody("isIdUsed[3]",equalTo("2"))
		.expectBody("termsConditionsAcceptedVersion[3]",equalTo("1.1"))
		.expectBody("termsConditionsAcceptedTimestamp[3]",equalTo("Dec 19, 2014, 11:41 AM (EST)"))
		.expectBody("enterpriseEmail[3]",equalTo("BFCRANE@YAHOO.COM"))
		.expectBody("bonusPoints[3]",equalTo("0"))
		.expectBody("newTeacher[3]",equalTo("0"))
		.expectBody("modifiedDate[3]",equalTo("20150703"))
		.expectBody("registrationDate[3]",equalTo("Dec 19, 2014, 11:41 AM (EST)"))
		.expectBody("firstLoginSchoolYear[3]",isEmptyString())
		.expectBody("mentorTeacher[3]",equalTo("1"))
		.expectBody("usedBookclubs[3]",equalTo("0"))
		.expectBody("gradeClasssizeId[3]",hasItem(""))
		.expectBody("privacyPolicyAcceptedTimestamp[3]",equalTo("Dec 19, 2014, 11:41 AM (EST)"))
		.expectBody("doNotRentDate[3]",isEmptyString())
		.expectBody("isEnabled[3]",equalTo("1"))
		.expectBody("userName[3]",equalTo("bfcrane@yahoo.com"))
		.expectBody("password[3]",equalTo("RgMZZp7/raQ="))
		.expectBody("spsId[3]",equalTo("62983507"));
		return rspec.build();
	}			
	private ResponseSpecification getAlternateTeacherResponseValidator()
	{
			
	   ResponseSpecBuilder rspec=new ResponseSpecBuilder()
	   .expectBody("schoolId[0]",equalTo("163392"))
	   .expectBody("schoolUcn[0]",equalTo("600078985"))
	   .expectBody("orgZip[0]",equalTo("11426"))
	   .expectBody("positions[0]",hasItems(""))
	   .expectBody("birthMonth[0]",equalTo("03"))
	   .expectBody("birthDay[0]",equalTo("16"))
	   .expectBody("schlAppUserType[0]",hasItem("2"))
	   .expectBody("readingLevel[0]",equalTo("20"))
	   .expectBody("salutation[0]",equalTo("MR"))
	   .expectBody("grades[0]",hasItem(""))
	   .expectBody("email[0]",equalTo("mrahman@scholastic.com"))
	   .expectBody("firstName[0]",equalTo("Mohammed"))
	   .expectBody("lastName[0]",equalTo("Rahman"))
	   .expectBody("bcoe[0]",equalTo("2165836095"))
	   .expectBody("cac[0]",equalTo("L2WN7"))
	   .expectBody("corpStatus[0]",isEmptyString())
	   .expectBody("privacyPolicyAcceptedVersion[0]",equalTo("1.1"))
	   .expectBody("userType[0]",hasItem("TEACHER"))
	   .expectBody("passwordReset[0]",equalTo("0"))
	   .expectBody("enterpriseEmail[0]",isEmptyString())
	   .expectBody("delivStatus[0]",equalTo("UNDELIVERABLE"))
	   .expectBody("newTeacher[0]",equalTo("1"))
	   .expectBody("modifiedDate[0]",equalTo("20160506"))
	   .expectBody("registrationDate[0]",equalTo("Jan 12, 2007, 0:19 PM (EST)"))
	   .expectBody("altTeacher[0]",equalTo("1"))
	   .expectBody("appSourceCode[0]",isEmptyString())
	   .expectBody("bonusPointsYtd[0]",equalTo("0"))
	   .expectBody("isEducator[0]",equalTo("1"))
	   .expectBody("subscriptionCool[0]",equalTo("1"))
	   .expectBody("subscriptionEducator[0]",equalTo("1"))
	   .expectBody("childUserIds[0]",hasItem("4990244"))
	   .expectBody("cacId[0]",equalTo("7988845"))
	   .expectBody("isIdUsed[0]",equalTo("2"))
	   .expectBody("termsConditionsAcceptedVersion[0]",equalTo("1.1"))
	   .expectBody("termsConditionsAcceptedTimestamp[0]",equalTo("Jul 10, 2014, 10:06 AM (EDT)"))
	   .expectBody("alsoParent[0]",equalTo("1"))
	   .expectBody("mentorTeacher[0]",equalTo("0"))
	   .expectBody("usedBookclubs[0]",equalTo("0"))
	   .expectBody("gradeClasssizeId[0]",hasItem(""))
	   .expectBody("firstLoginSchoolYear[0]",isEmptyString())
	   .expectBody("ucn[0]",equalTo("630437264"))
	   .expectBody("emailCaptureDate[0]",equalTo("2007-01-12"))
	   .expectBody("level1RegistrationDate[0]",equalTo("Jan 12, 2007, 0:19 PM (EST)"))
	   .expectBody("lastmodifiedDate[0]",equalTo("May 06, 2016, 2:45 PM (EDT)"))
	   .expectBody("privacyPolicyAcceptedTimestamp[0]",equalTo("Jul 10, 2014, 10:06 AM (EDT)"))
	   .expectBody("isEnabled[0]",equalTo("1"))
	   .expectBody("password[0]",equalTo("kgGL+xsw4gg="))
	   .expectBody("note[0]",hasItems("4676955|scsToolsApp|Jul 17, 2009, 8:14:02 AM (GMT-05:00)|SYS: Modification:<br>Bonus Points YTD: [null]->[]",
	  				    			  "4676955|myAccountApp|Jan 12, 2007, 0:19:06 PM (EST)|SYS: Creation", 
	  				    			  "4676955|MyAccountWebApp|Aug 06, 2011, 10:32:35 AM (EDT)|SYS: Modification:<br>Capture Date"))
	   .expectBody("userName[0]",equalTo("zr700"))
	   .expectBody("spsId[0]",equalTo("4676955"));
	   return rspec.build();
    }	
		
	private ResponseSpecification getPrimarySchoolAddressResponseValidator()
	{ 
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("addressTypeCode",equalTo("05"))
		.expectBody("uspsTypeCode",equalTo("2"))
		.expectBody("borderId",equalTo("11426020"))
		.expectBody("creditCardState",equalTo("NY"))
		.expectBody("zipcode",equalTo("11426"))
		.expectBody("poBox",equalTo("0"))
		.expectBody("schoolStatus",equalTo("1"))
		.expectBody("groupType",equalTo("S_6E"))
		.expectBody("internatIonalDomestIcCode",equalTo("01"))
		.expectBody("publicPrivateKey",equalTo("01"))
		.expectBody("reportingSchoolType",equalTo("0"))
		.expectBody("createDate",equalTo("2004-05-22 10:20:03.987695"))
		.expectBody("createdSource",equalTo("DATA DUMP"))
		.expectBody("address1",equalTo("24805 86TH AVE"))
		.expectBody("address2",isEmptyString())
		.expectBody("phone",equalTo("7188314016|0"))
		.expectBody("city",equalTo("BELLEROSE"))
		.expectBody("country",equalTo("US"))
		.expectBody("schoolUCN",equalTo("600078985"))
		.expectBody("name",equalTo("PS 133 QUEENS SCHOOL"))
		.expectBody("state",equalTo("NY"))
		.expectBody("spsId",equalTo("163392"));
		return rspec.build();
     }
		
	 private ResponseSpecification getSchoolByZipcodeResponseValidator()
	 {
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("creditCardState[0]",isEmptyString())
		.expectBody("zipcode[0]",equalTo("11431"))
		.expectBody("schoolStatus[0]",equalTo("1"))
		.expectBody("groupType[0]",equalTo("I_CD"))
		.expectBody("address1[0]",equalTo("1 MIDAN DIGLA"))
		.expectBody("city[0]",equalTo("CAIRO"))
		.expectBody("schoolUCN[0]",equalTo("600223127"))
		.expectBody("name[0]",equalTo("CAIRO AMER COLLEGE"))
		.expectBody("state[0]",isEmptyString())
		.expectBody("spsId[0]",equalTo("30128438"))

		.expectBody("creditCardState[1]",equalTo("NY"))
		.expectBody("zipcode[1]",equalTo("11431"))
		.expectBody("schoolStatus[1]",equalTo("1"))
		.expectBody("groupType[1]",equalTo("EC_PS"))
		.expectBody("address1[1]",equalTo("18710 HILLSIDE AVE"))
		.expectBody("city[1]",equalTo("JAMAICA"))
		.expectBody("schoolUCN[1]",equalTo("610166776"))
		.expectBody("name[1]",equalTo("EARLY SUNRISE PRE - SCH AND KN"))
		.expectBody("state[1]",equalTo("NY"))
		.expectBody("spsId[1]",equalTo("30160821"));
		return rspec.build();
	 }    			
		
	 private ResponseSpecification getCreditCardWalletResponseValidator()
	 {
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("count",equalTo(1))
		.rootPath("wallet")
		.expectBody("id[0]",equalTo("0"))
		.expectBody("ccmonth[0]",equalTo("06"))
		.expectBody("cbspt[0]",equalTo("0013552898373282"))
		.expectBody("cnm[0]",equalTo("xxxxxxxxxxx3282"))
		.expectBody("phoneExtNumber[0]",isEmptyString())
		.expectBody("email[0]",isEmptyString())
		.expectBody("zip[0]",equalTo("10012"))
		.expectBody("firstName[0]",equalTo("Bhavani"))
		.expectBody("address1[0]",equalTo("557 Broadway"))
		.expectBody("address2[0]",isEmptyString())
		.expectBody("ccBrand[0]",equalTo("A"))
		.expectBody("phoneNumber[0]",isEmptyString())
		.expectBody("lastName[0]",equalTo("Mekala"))
		.expectBody("ccnumber[0]",equalTo("xxxxxxxxxxx3282"))
		.expectBody("city[0]",equalTo("New York"))
		.expectBody("success[0]",equalTo("test"))
		.expectBody("default[0]",equalTo("false"))
		.expectBody("state[0]",equalTo("NY"));
		return rspec.build();
	 }
	 
	 private ResponseSpecification getAllGradeClassSizeResponseValidator()
	 {
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("spsUserId",hasItem("68285633"))
		.expectBody("spsId",hasItem("4215605"))
		.expectBody("classSize",hasItem("23"))
		.expectBody("grade",hasItem("04"));		
		return rspec.build();
	 }
		
	 private ResponseSpecification getChildUsersResponseValidator()
	 {
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("birthYear",equalTo("2011"))
		.expectBody("cac",isEmptyString())
		.expectBody("firstName",equalTo("John"))
		.expectBody("lastName",equalTo("Mandale"))
		.expectBody("parentIds",hasItem("4676955"))
		.expectBody("teacherIds",hasItem(""))
		.expectBody("birthMonth",equalTo("02"))
		.expectBody("isLinkedToALtTeacher",isEmptyString())
		.expectBody("classroomTeacher",isEmptyString())
		.expectBody("isFat",isEmptyString())
		.expectBody("coolId",isEmptyString())
		.expectBody("grade",equalTo("02"))
		.expectBody("gender",equalTo("M"))
		.expectBody("spsId",equalTo("4990244"));
		return rspec.build();
	}

/*private ResponseSpecification getExistingAssociatedSocialAccountsResponseValidator()

	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("[0]",equalTo("facebook"))
		.expectBody("[1]",equalTo("googleplus"));
		return rspec.build();
	}*/

		private static void addKeyValueValidations(ResponseSpecBuilder specBuilder,	String[][] keyValues) 
		{
			specBuilder.expectBody("size()", equalTo(keyValues.length));
			for (int i = 0; i < keyValues.length; i++) 
			{
				specBuilder.expectBody("get(" + i + ").key",
				equalTo(keyValues[i][0]));
				specBuilder.expectBody("get(" + i + ").name",
				equalTo(keyValues[i][1]));
			}
		}
		
		

}
