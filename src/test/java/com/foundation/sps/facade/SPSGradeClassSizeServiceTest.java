package com.foundation.sps.facade;

import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;

public class SPSGradeClassSizeServiceTest extends BaseTest
{

	//Login for Session Id and TSP Id
	private String user;
	//private String password ="passed1";
	private boolean singleToken = true;
	private boolean userForWS = false;
	private boolean addCookie = true;
	private boolean addExpiration = true;
	private String sps_session;
	private String sps_tsp;
	private String userSPSID;

	// Teacher Info
	private String teacherfirstName="Andrew";
	private String teacherlastName="Gilbert";
	private String email;
	private String password="passed1";
	private String schoolId="30755883";
	private List<String> userType1=new ArrayList<String>();
	private String teacherSPSID;
	//private boolean firstYearTeaching = true;
	
	//GradeClassSize Info
	private String grade="02";
	//private List<String> classSize=new ArrayList<String>();
	private String classSize="25";
	private String gradeclasssizeSPSID;
	
	//Update GradeClassSize Info
	private String grade1="04";
	private String classSize1="21";
	
	//End Points
	private static final String ENDPOINT_TEACHER_REGISTRATION="/spsuser/?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_TEACHER_DELETE="/spsuser/{spsId}?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_CREATE_GRADECLASSSIZE="/spsgradeclasssize/?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_GET_UPDATE_DELETE_GRADECLASSSIZE="/spsgradeclasssize/{spsId}?clientId=SPSFacadeAPI";
	
 @Test
	public void createGradeClassSizeGetGradeClassSizeTest()
	{
		System.out.println("###############################################################################################");	
		System.out.println("*******  Create Teacher, Crate GradeClassSize, Get the GradeClassSize and Delete Teacher*******");
		System.out.println("###############################################################################################");
		
		// Light Teacher Registration
		ExtractableResponse<Response> createTeacherResponse=
							given()
									.log().all()
									.header("Authorization", String.format("Bearer %s",getAccesToken()))
									.contentType("application/json")
									.body(createTeacherPayload()).
							when()
									.post(ENDPOINT_TEACHER_REGISTRATION).
							then()
									.statusCode(201)
									.spec(createTeacherResponseValidator())
									.extract();		
		teacherSPSID=createTeacherResponse.path("spsId");	
		System.out.println(teacherSPSID);
					
		// Create GradeClassSize
		ExtractableResponse<Response> createGradeClassSizeResponse=
							given()
									.log().all()
									.header("Authorization", String.format("Bearer %s",getAccesToken()))
									.contentType("application/json")
									.body(createGradeClassSizePayload()).
							when()
									.post(ENDPOINT_CREATE_GRADECLASSSIZE).
							then()
									.statusCode(200)
									.spec(createGradeClassSizeResponseValidator())
									.extract();
		gradeclasssizeSPSID=createGradeClassSizeResponse.path("spsId");	
		System.out.println("gradeclasssizeSPSID is: "+gradeclasssizeSPSID);
		
		// Get GradeClassSize 
		ExtractableResponse<Response> getGradeClassSizeResponse=
							given()
									.header("Authorization", String.format("Bearer %s",getAccesToken()))
									.pathParam("spsId",gradeclasssizeSPSID).
							when()
									.get(ENDPOINT_GET_UPDATE_DELETE_GRADECLASSSIZE).
							then()
									.statusCode(200)
									.extract();	
		System.out.println("********** Grade Class Size Info **********");
		System.out.println(getGradeClassSizeResponse.asString());
		System.out.println("********************************************");
		
		// Delete Teacher 
		ExtractableResponse<Response> deleteTeacherResponse=
							given()
									.log().all()
									.pathParam("spsId",teacherSPSID)
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.delete(ENDPOINT_TEACHER_DELETE).
							then()
									.statusCode(200)
									.extract();	
		System.out.println ("************** After Deleting Teacher *****************");
		System.out.println(deleteTeacherResponse.asString());
	}	
	
 @Test
	public void updateGradeClassSizeGetGradeClassSizeTest()
	{
		System.out.println("###############################################################################################");	
		System.out.println("******* Create GradeClassSize, Update and Get the GradeClassSize then Delete the Teacher*******");
		System.out.println("###############################################################################################");
		
		// Light Teacher Registration
		ExtractableResponse<Response> createTeacherResponse=
							given()
									.log().all()
									.header("Authorization", String.format("Bearer %s",getAccesToken()))
									.contentType("application/json")
									.body(createTeacherPayload()).
							when()
									.post(ENDPOINT_TEACHER_REGISTRATION).
							then()
									.statusCode(201)
									.spec(createTeacherResponseValidator())
									.extract();		
		teacherSPSID=createTeacherResponse.path("spsId");	
		System.out.println(teacherSPSID);
					
		// Create GradeClassSize
		ExtractableResponse<Response> createGradeClassSizeResponse=
							given()
									.log().all()
									.header("Authorization", String.format("Bearer %s",getAccesToken()))
									.contentType("application/json")
									.body(createGradeClassSizePayload()).
							when()
									.post(ENDPOINT_CREATE_GRADECLASSSIZE).
							then()
									.statusCode(200)
									.spec(createGradeClassSizeResponseValidator())
									.extract();
		gradeclasssizeSPSID=createGradeClassSizeResponse.path("spsId");	
		System.out.println("gradeclasssizeSPSID is: "+gradeclasssizeSPSID);
				
		// Update GradeClassSize
		ExtractableResponse<Response> updateGradeClassSizeResponse=
							given()
									.log().all()
									.header("Authorization", String.format("Bearer %s",getAccesToken()))
									.pathParam("spsId",gradeclasssizeSPSID)
									.contentType("application/json")
									.body(updateGradeClassSizePayload()).
							when()
									.put(ENDPOINT_GET_UPDATE_DELETE_GRADECLASSSIZE).
							then()
									.statusCode(200)
									.spec(updateGradeClassSizeResponseValidator())
									.extract();
		gradeclasssizeSPSID=updateGradeClassSizeResponse.path("spsId");	
		System.out.println("gradeclasssizeSPSID is: "+gradeclasssizeSPSID);	
		
		// Get GradeClassSize 
		ExtractableResponse<Response> getUpdatedGradeClassSizeResponse=
							given()
									.header("Authorization", String.format("Bearer %s",getAccesToken()))
									.pathParam("spsId",gradeclasssizeSPSID).
							when()
									.get(ENDPOINT_GET_UPDATE_DELETE_GRADECLASSSIZE).
							then()
									.statusCode(200)
									.extract();	
		System.out.println("********** Updated Grade Class Size Info **********");
		System.out.println(getUpdatedGradeClassSizeResponse.asString());
		System.out.println("***************************************************");
		
		System.out.println("********** Grade Class Size Info **********");
		System.out.println(createGradeClassSizeResponse.asString());
		System.out.println("***************************************************");
		
		// Delete Teacher 
		ExtractableResponse<Response> deleteTeacherResponse=
							given()
									.log().all()
									.pathParam("spsId",teacherSPSID)
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.delete(ENDPOINT_TEACHER_DELETE).
							then()
									.statusCode(200)
									.extract();	
		System.out.println ("************** After Deleting Teacher *****************");
		System.out.println(deleteTeacherResponse.asString());
	}	
	
 @Test
	public void deleteGradeClassSizeGetGradeClassSizeTest()
	{
		System.out.println("######################################################################################");	
		System.out.println("******* Create GradeClassSize and Delete the ClassSize aswell as Teacher also. *******");
		System.out.println("######################################################################################");
		
		// Light Teacher Registration
		ExtractableResponse<Response> createTeacherResponse=
							given()
									.log().all()
									.header("Authorization", String.format("Bearer %s",getAccesToken()))
									.contentType("application/json")
									.body(createTeacherPayload()).
							when()
									.post(ENDPOINT_TEACHER_REGISTRATION).
							then()
									.statusCode(201)
									.spec(createTeacherResponseValidator())
									.extract();		
		teacherSPSID=createTeacherResponse.path("spsId");	
		System.out.println(teacherSPSID);
					
		// Create GradeClassSize
		ExtractableResponse<Response> createGradeClassSizeResponse=
							given()
									.log().all()
									.header("Authorization", String.format("Bearer %s",getAccesToken()))
									.contentType("application/json")
									.body(createGradeClassSizePayload()).
							when()
									.post(ENDPOINT_CREATE_GRADECLASSSIZE).
							then()
									.statusCode(200)
									.spec(createGradeClassSizeResponseValidator())
									.extract();
		gradeclasssizeSPSID=createGradeClassSizeResponse.path("spsId");	
		System.out.println("gradeclasssizeSPSID is: "+gradeclasssizeSPSID);
				
		// Delete GradeClassSize
		ExtractableResponse<Response> deleteGradeClassSizeResponse=
							given()
									.log().all()
									.header("Authorization", String.format("Bearer %s",getAccesToken()))
									.pathParam("spsId",gradeclasssizeSPSID)
									.contentType("application/json").
							when()
									.delete(ENDPOINT_GET_UPDATE_DELETE_GRADECLASSSIZE).
							then()
									.statusCode(200)
									.extract();
				
		System.out.println("********** Deleted Grade Class Size ***************");
		System.out.println(deleteGradeClassSizeResponse.asString());
		System.out.println("***************************************************");
		
		// Delete Teacher 
		ExtractableResponse<Response> deleteTeacherResponse=
							given()
									.log().all()
									.pathParam("spsId",teacherSPSID)
									.header("Authorization", String.format("Bearer %s",getAccesToken())).
							when()
									.delete(ENDPOINT_TEACHER_DELETE).
							then()
									.statusCode(200)
									.extract();	
		System.out.println ("************** After Deleting Teacher *****************");
		System.out.println(deleteTeacherResponse.asString());
			
	}
 
 @Test
	public void createTeacherLogInTest() throws InterruptedException
 	{
	 	System.out.println("######################################################################################");	
	 	System.out.println("*************** Create User (Teacher), Authenticate and Delete the User***************");
	 	System.out.println("######################################################################################");
	 	
	 	//Create User(Teacher)
	 	ExtractableResponse<Response> createTeacherResponse=
	 						given()
	 								.log().all()
	 								.header("Authorization", String.format("Bearer %s",getAccesToken()))
	 								.contentType("application/json")
	 								.body(createTeacherPayload()).
							when()
									.post(ENDPOINT_TEACHER_REGISTRATION).
							then()
									.statusCode(201)
									.spec(createTeacherResponseValidator())
									.extract();		
	 	teacherSPSID=createTeacherResponse.path("spsId");
	 	user=createTeacherResponse.path("userName");
	 	System.out.println(">>>>>>>Teacher SPSID is: "+teacherSPSID);
	 	System.out.println(">>>>>>>User Name is: "+user);
	
	 	Thread.sleep(6000L);
	
	 	//Authentication
	 	ExtractableResponse<Response> createLogInResponse=
	 						given()
	 								.header("Authorization", String.format("Bearer %s",getAccesToken()))
	 								.contentType("application/json")
	 								.body(createLogInPayload()).
	                		when()
	                				.post("https://api.scholastic.com/sps-api-facade/1.0/spsuser/login?clientId=SPSFacadeAPI").
							then()
									.statusCode(200)
									.extract();	
	 	sps_session=createLogInResponse.path("SPS_SESSION.value");
	 	sps_tsp=createLogInResponse.path("SPS_TSP.value");
	 	System.out.println(createLogInResponse.path("SPS_UD.value"));
	 	userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
	
	 	System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
	 	System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
	 	System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
	 	
	 	// Delete Teacher 
	 	ExtractableResponse<Response> deleteTeacherResponse=
	 						given()
	 								.log().all()
	 								.pathParam("spsId",teacherSPSID)
	 								.header("Authorization", String.format("Bearer %s",getAccesToken())).
	 						when()
	 								.delete(ENDPOINT_TEACHER_DELETE).
	 						then()
	 								.statusCode(200)
	 								.extract();	
	 		System.out.println ("************** After Deleting Teacher *****************");
	 		System.out.println(deleteTeacherResponse.asString());
  	}
 
 
 
	private String getEmail()
	{
		email="sps_prod"+String.valueOf(System.currentTimeMillis())+"@sample.com";
		return email;
	
	}
	
	private Map<String, Object> createTeacherPayload()
	
	{
		Map<String, Object> teacherRegistrationInfo = new HashMap<String, Object>();
		teacherRegistrationInfo.put("firstName",teacherfirstName);
		teacherRegistrationInfo.put("lastName",teacherlastName);
		teacherRegistrationInfo.put("email",getEmail());
		userType1.add("TEACHER");
		teacherRegistrationInfo.put("userType", userType1);
		teacherRegistrationInfo.put("password",password);
		teacherRegistrationInfo.put("userName",email);
		teacherRegistrationInfo.put("schoolId",schoolId);
		return teacherRegistrationInfo;
		
	}

	private Map<String, Object> createGradeClassSizePayload()
			
	{
		Map<String, Object> gradeClassSizeInfo = new HashMap<String, Object>();
		gradeClassSizeInfo.put("grade",grade);
		gradeClassSizeInfo.put("classSize",classSize);
		gradeClassSizeInfo.put("spsUserId",teacherSPSID);
		return gradeClassSizeInfo;
		
	}
	
	private Map<String, Object> updateGradeClassSizePayload()
	
	{
		Map<String, Object> gradeClassSizeUpdateInfo = new HashMap<String, Object>();
		gradeClassSizeUpdateInfo.put("grade",grade1);
		gradeClassSizeUpdateInfo.put("classSize",classSize1);
		gradeClassSizeUpdateInfo.put("spsUserId",teacherSPSID);
		return gradeClassSizeUpdateInfo;
		
	}
	
	private ResponseSpecification createTeacherResponseValidator()
	
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("userName",equalTo(email))
		//.expectBody("modifiedDate",equalTo(""))
		//.expectBody("registrationDate",equalTo(new Date()))
		.expectBody("schoolId",equalTo(schoolId))
		.expectBody("orgZip",is(not(empty())))
		.expectBody("userType",equalTo(userType1))
		.expectBody("isEducator",is(not(empty())))
		.expectBody("cac",is(not(empty())))
		.expectBody("cacId",is(not(empty())))
		.expectBody("isIdUsed",is(not(empty())))
		.expectBody("isEnabled",is(not(empty())))
		.expectBody("schoolUcn",is(not(empty())))
		.expectBody("email",equalTo(email))
		.expectBody("firstName",equalTo(teacherfirstName))
		.expectBody("lastName",equalTo(teacherlastName))
		//.expectBody("password",equalTo(password))
		.expectBody("spsId",is(not(empty())));
		return rspec.build();		
	}
	
	private ResponseSpecification createGradeClassSizeResponseValidator()
	
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("grade",equalTo(grade))
		.expectBody("classSize",equalTo(classSize))
		.expectBody("spsUserId",equalTo(teacherSPSID))
		.expectBody("spsId",is(not(empty())));
		return rspec.build();		
	}
	
	private ResponseSpecification updateGradeClassSizeResponseValidator()
	
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("grade",equalTo(grade1))
		.expectBody("classSize",equalTo(classSize1))
		.expectBody("spsUserId",equalTo(teacherSPSID))
		.expectBody("spsId",is(not(empty())));
		return rspec.build();
	}	

	private Map<String, Object> createLogInPayload()

	{
		Map<String, Object> createLogInInfo = new HashMap<String, Object>();
		createLogInInfo.put("user",user);
		createLogInInfo.put("password",password);
		createLogInInfo.put("singleToken",singleToken);
		createLogInInfo.put("userForWS", userForWS);
		createLogInInfo.put("addCookie",addCookie);
		createLogInInfo.put("addExpiration",addExpiration);
		//System.out.println("!!!!!!!!!!!!!"+createLogInInfo.toString());
		return createLogInInfo;

	}
}
